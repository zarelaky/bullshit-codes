    @Override
    @Transactional(rollbackFor = Exception.class)
    public ResponseResult trajectorys(TbSupportTeams tbSupportTeams, LoginUser loginUser) throws Exception {
        try {
            if (null == tbSupportTeams.getTbSupportTeamList() || tbSupportTeams.getTbSupportTeamList().size() == 0) {
                return ResponseResult.error("当前团队未发生改变！");
            }

            Long culeId = tbSupportTeams.getTbSupportTeamList().get(0).getClueId();
            if (null == culeId) {
                return ResponseResult.error("当前请求参数不能为空");
            }

            TbDictProject project = projectService.getOne(new QueryWrapper<TbDictProject>().eq("clue_id", culeId).last("limit 1"));
            if (null != project && null != project.getTempId()) {
                if (project.getTempId() == 1 && project.getProjectPhaseId() < 4) {
                    return ResponseResult.error("当前环节不能添加成员");
                } else if (project.getTempId() == 2 && project.getProjectPhaseId() < 204) {
                    return ResponseResult.error("当前环节不能添加成员");
                } else if (project.getTempId() == 3 && project.getProjectPhaseId() < 304) {
                    return ResponseResult.error("当前环节不能添加成员");
                } else if (project.getTempId() == 4 && project.getProjectPhaseId() < 404) {
                    return ResponseResult.error("当前环节不能添加成员");
                } else if (project.getTempId() == 6 && project.getProjectPhaseId() < 604) {
                    return ResponseResult.error("当前环节不能添加成员");
                }
            } else {
                return ResponseResult.error("当前环节无法组建团队");
            }

            List<TbSupportTeam> coreHaveTeams = tbSupportTeamMapper.selectSupportTeamList(culeId);
            if (null == coreHaveTeams || coreHaveTeams.size() == 0) {
                return ResponseResult.error("团队成员信息错误，请联系管理员");
            }

            List<TbSupportTeam> coreTeamList = tbSupportTeams.getTbSupportTeamList().stream().filter(item -> 1 == item.getTeamGroupId()).collect(Collectors.toList());
            for (TbSupportTeam tbSupportTeam : coreTeamList) {
                TbSupportTeam supportTeam = coreHaveTeams.stream().filter(item -> tbSupportTeam.getMemRole().equals(item.getMemRole()) && tbSupportTeam.getMenAscription().equals(item.getMenAscription())).findAny().orElse(null);
                String memRle = null;
                String menAscription;
                if ("888".equals(tbSupportTeam.getMenAscription())) {
                    menAscription = "集团总公司";
                } else {
                    menAscription = "分公司";
                }
                switch (tbSupportTeam.getMemRole()) {
                    case Constants.SOLUTIONMANAGER:
                        memRle = "解决方案经理";
                        break;
                    case Constants.JFJL:
                        memRle = "交付经理";
                        break;
                    case Constants.YYJL:
                        memRle = "运营经理";
                        break;
                    case Constants.SWJL:
                        memRle = "商务经理";
                        break;
                    case Constants.YFRY:
                        memRle = "研发人员";
                        break;
                }
                if (null != supportTeam) {
                    if (null != memRle) {
                        return ResponseResult.error(menAscription + memRle + "已经存在！");
                    }
                }
            }

            List<TbSupportTeam> tbSupportTeamList = new ArrayList<>();
            List<TbSupportTeam> xmManagerList = tbSupportTeams.getTbSupportTeamList().stream().filter(item -> 1 == item.getTeamGroupId()).collect(Collectors.toList());
            List<TbSupportTeam> solutionManagerList = tbSupportTeams.getTbSupportTeamList().stream().filter(item -> 2 == item.getTeamGroupId()).collect(Collectors.toList());
            List<TbSupportTeam> jfjlList = tbSupportTeams.getTbSupportTeamList().stream().filter(item -> 3 == item.getTeamGroupId()).collect(Collectors.toList());
            List<TbSupportTeam> yyjlList = tbSupportTeams.getTbSupportTeamList().stream().filter(item -> 4 == item.getTeamGroupId()).collect(Collectors.toList());

            List<TbSupportTeam> teams = tbSupportTeamMapper.selectSupportTeams(loginUser.getUsername(), tbSupportTeams.getTbSupportTeamList().get(0).getProjectId());
            if ("1".equals(tbSupportTeams.getLink())) {
                if (jfjlList.size() > 0 || yyjlList.size() > 0) {
                    return ResponseResult.error("当前环节不允许添加团队成员！");
                }
            }
            if ("2".equals(tbSupportTeams.getLink())) {
                if (yyjlList.size() > 0) {
                    return ResponseResult.error("当前环节不允许添加团队成员！");
                }
            }
            if (null != teams && teams.size() > 0) {
                List<String> collect = teams.stream().filter(item -> loginUser.getUsername().equals(item.getMemberId())).map(TbSupportTeam::getMemRole).collect(Collectors.toList());
                boolean solutionManager = collect.contains(Constants.SOLUTIONMANAGER);
                boolean jfjl = collect.contains(Constants.JFJL);
                boolean yyjl = collect.contains(Constants.YYJL);
                List<String> transactorList = null;

                tbSupportTeamList.addAll(xmManagerList);
                if (solutionManager) {
                    tbSupportTeamList.addAll(solutionManagerList);
                    if ("1".equals(tbSupportTeams.getLink())) {
                        transactorList = teams.stream().filter(item -> Constants.SOLUTIONMANAGER.equals(item.getMemRole())).map(TbSupportTeam::getMemberId).collect(Collectors.toList());
                    }
                }
                if (jfjl) {
                    tbSupportTeamList.addAll(jfjlList);
                    if ("2".equals(tbSupportTeams.getLink())) {
                        transactorList = teams.stream().filter(item -> Constants.JFJL.equals(item.getMemRole())).map(TbSupportTeam::getMemberId).collect(Collectors.toList());
                    }
                }
                if (yyjl) {
                    tbSupportTeamList.addAll(yyjlList);
                    if ("3".equals(tbSupportTeams.getLink())) {
                        transactorList = teams.stream().filter(item -> Constants.YYJL.equals(item.getMemRole())).map(TbSupportTeam::getMemberId).collect(Collectors.toList());
                    }
                }

                Integer link = workbenchMapper.selectTbProjectTrack(culeId);
                Integer tempId = project.getTempId();
                int solutionManagerLink = 0;
                int jfjlLink = 0;
                int yyjlLink = 0;
                int contractAwardLink = 9;
                switch (tempId) {
                    case 1:
                        solutionManagerLink = 4;
                        jfjlLink = 11;
                        yyjlLink = 18;
                        contractAwardLink = 9;
                        break;
                    case 2:
                        solutionManagerLink = 4;
                        jfjlLink = 8;
                        yyjlLink = 16;
                        contractAwardLink = 14;
                        break;
                    case 3:
                        solutionManagerLink = 4;
                        jfjlLink = 9;
                        yyjlLink = 16;
                        contractAwardLink = 7;
                        break;
                    case 4:
                        solutionManagerLink = 4;
                        jfjlLink = 7;
                        yyjlLink = 14;
                        contractAwardLink = 5;
                        break;
                    case 6:
                        solutionManagerLink = 4;
                        jfjlLink = 9;
                        yyjlLink = 16;
                        contractAwardLink = 7;
                        break;
                }
                if ((link + 1) == contractAwardLink) {
                    teams = tbSupportTeamMapper.selectSupportTeams(null, tbSupportTeams.getTbSupportTeamList().get(0).getProjectId());
                    transactorList = teams.stream().filter(item -> Constants.CUSTOMERMANAGER.equals(item.getMemRole())).map(TbSupportTeam::getMemberId).collect(Collectors.toList());
                }
                if ("1".equals(tbSupportTeams.getLink()) && solutionManagerLink == link) {
                    if (solutionManager) {
                        projectTrackService.updateTbProjectTracks(culeId, 1, loginUser.getUsername());
                        projectTrackService.insertTbProjectTracks(culeId, transactorList, 0, 0, null);
                    } else {
                        return ResponseResult.error("您不是解决方案经理，不能添加成员");
                    }
                } else if ("2".equals(tbSupportTeams.getLink()) && jfjlLink == link) {
                    if (jfjl) {
                        projectTrackService.updateTbProjectTracks(culeId, 1, loginUser.getUsername());
                        projectTrackService.insertTbProjectTracks(culeId, transactorList, 0, 0, null);
                    } else {
                        return ResponseResult.error("您不是交付经理，不能添加成员");
                    }
                } else if ("3".equals(tbSupportTeams.getLink()) && yyjlLink == link) {
                    if (yyjl) {
                        projectTrackService.updateTbProjectTracks(culeId, 1, loginUser.getUsername());
                        projectTrackService.insertTbProjectTracks(culeId, transactorList, 0, 0, null);
                    } else {
                        return ResponseResult.error("您不是运营经理，不能添加成员");
                    }
                }
            }
            tbSupportTeamList.forEach(e -> {
                e.setCreater(loginUser.getUsername());
                e.setCreateTime(new Date());
            });
            this.saveBatch(tbSupportTeamList);
            return ResponseResult.success();
        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return ResponseResult.error("组建团队失败，请联系管理员！");
        }
    }