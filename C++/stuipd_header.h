//在头文件里展开命名空间, 将导致源文件内出现莫名其妙的命名冲突
using namespace std;
//在头文件里定义变量, 将导致源文件内出现重复定义
int var1;

class NewClassName
{
private:

protected:

public:
NewClassName() {}
~NewClassName() {}
//class定义最后缺少分号, 这个错误将导致报错定位在c文件里而非头文件里
}